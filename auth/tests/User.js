var expect = require('expect.js')
  , _ = require('lodash')
  , Promise = require('bluebird');

var db = require('knex')({
    client: 'sqlite3',
    connection: {
      filename: ':memory:'
    }
  })
  , User = require('../models/User').bindDb(db)
  , ValidationError = require('yessql-core/errors/ValidationError');

describe('User', function () {

  var user = null;
  var admin = null;
  var userPassword = '12352463563';
  var adminPassword = 'adSGA53SA29';

  before(function () {
    return db.schema
      .hasTable('User')
      .then(function (hasTable) {
        if (!hasTable) {
          return db.schema.createTable(User.tableName, function (table) {
            table.increments('id').primary();
            table.string('username').index().unique();
            table.string('firstName');
            table.string('lastName');
            table.string('email');
            table.text('roles').notNullable();
            table.string('passwordHash', 512).notNullable();
            table.string('passwordSalt', 256).notNullable();
          });
        }
      });
  });

  after(function () {
    return db.schema
      .hasTable('User')
      .then(function (hasTable) {
        if (hasTable) {
          return db.schema.dropTableIfExists(User.tableName);
        }
      });
  });

  it('should create user', function () {

    user = User.fromJson({
      username: 'user',
      email: 'user@user.com',
      password: userPassword,
      roles: [User.Role.User],
      firstName: null,
      lastName: null
    });

    return user.$insert().then(function (user) {
      return db('User').where('id', user.id);
    }).then(function (rows) {
      expect(rows[0]).to.eql(user.$toDatabaseJson());
      return User.findByUsername(user.username);
    }).then(function (model) {
      expect(model.toJSON()).to.eql(user.toJSON());
      return User.findOne().where('id', user.id);
    }).then(function (model) {
      expect(model.toJSON()).to.eql(user.toJSON());
    });
  });

  it('should create admin', function () {

    admin = User.fromJson({
      username: 'admin',
      email: 'admin@admin.com',
      password: adminPassword,
      roles: [User.Role.Admin],
      firstName: null,
      lastName: null
    });

    return admin.$insert().then(function () {
      return db('User').where('id', '=', admin.id);
    }).then(function (rows) {
      expect(rows[0]).to.eql(admin.$toDatabaseJson());
      return User.findByUsername(admin.username);
    }).then(function (model) {
      expect(model.toJSON()).to.eql(admin.toJSON());
      return User.findOne().where('id', admin.id);
    }).then(function (model) {
      expect(model.toJSON()).to.eql(admin.toJSON());
    });
  });

  it('should update user', function () {
    user.firstName = 'User';
    return user.$update().then(function () {
      return db('User').where('id', '=', user.id);
    }).then(function (rows) {
      expect(rows[0]).to.eql(user.$toDatabaseJson());
      return User.findByUsername(user.username);
    }).then(function (model) {
      expect(model.toJSON()).to.eql(user.toJSON());
      return User.findOne().where({id: user.id});
    }).then(function (model) {
      expect(model.toJSON()).to.eql(user.toJSON());
    });
  });

  it('should fail to create user (no username)', function () {
    expect(function () {
      User.fromJson({
        password: 'password',
        roles: [User.Role.Admin]
      });
    }).to.throwException(function (error) {
      expect(error).to.be.a(ValidationError);
      expect(error).to.have.property('data');
      expect(error.data).to.have.property('username');
    });
  });

  it('should fail to create user (no password)', function () {
    expect(function () {
      User.fromJson({
        username: 'user',
        roles: [User.Role.Admin]
      });
    }).to.throwException(function (error) {
      expect(error).to.be.a(ValidationError);
      expect(error).to.have.property('data');
      expect(error.data).to.have.property('password');
    });
  });

  it('should fail to create user (password too short)', function () {
    expect(function () {
      User.fromJson({
        username: 'user',
        password: 'passw',
        roles: [User.Role.Admin]
      });
    }).to.throwException(function (error) {
      expect(error).to.be.a(ValidationError);
      expect(error).to.have.property('data');
      expect(error.data).to.have.property('password');
    });
  });

  it('should fail to create user (no roles)', function () {
    expect(function () {
      User.fromJson({
        username: 'user',
        password: 'password',
        roles: []
      });
    }).to.throwException(function (error) {
      expect(error).to.be.a(ValidationError);
      expect(error).to.have.property('data');
      expect(error.data).to.have.property('roles');
    });
  });

  it('should fail to create user (null roles)', function () {
    expect(function () {
      User.fromJson({
        username: 'user',
        password: 'password',
        roles: null
      });
    }).to.throwException(function (error) {
      expect(error).to.be.a(ValidationError);
      expect(error).to.have.property('data');
      expect(error.data).to.have.property('roles');
    });
  });

  it('should fail to create user (invalid roles)', function () {
    expect(function () {
      User.fromJson({
        username: 'user',
        password: 'password',
        roles: [User.Role.Admin, 982734]
      });
    }).to.throwException(function (error) {
      expect(error).to.be.a(ValidationError);
      expect(error).to.have.property('data');
      expect(error.data).to.have.property('roles.1');
    })
  });

  it('should match user\'s password', function () {
    return user.$isPasswordValid(userPassword).then(function (isValid) {
      expect(isValid).to.eql(true);
    });
  });

  it('should match admin\'s password', function () {
    return admin.$isPasswordValid(adminPassword).then(function (isValid) {
      expect(isValid).to.eql(true);
    });
  });

  it('admin\'s password should not match with user\'s', function () {
    return user.$isPasswordValid(adminPassword).then(function (isValid) {
      expect(isValid).to.eql(false);
    });
  });

  it('database JSON should not have password', function () {
    _.each([user, admin], function (usr) {
      _.each(usr.$toDatabaseJson(), function (value) {
        expect(value).not.to.eql(userPassword);
        expect(value).not.to.eql(adminPassword);
      });
    });
  });

  it('response JSON should not have password', function () {
    _.each([user, admin], function (usr) {
      expect(usr).to.have.property('password');
      _.each(usr.$toJson(), function (value) {
        expect(value).not.to.eql(userPassword);
        expect(value).not.to.eql(adminPassword);
      });
    });
  });

  it('response JSON should not have password salt', function () {
    _.each([user, admin], function (usr) {
      expect(usr).to.have.property('passwordSalt');
      _.each(usr.$toJson(), function (value) {
        expect(value).not.to.eql(user.passwordSalt);
        expect(value).not.to.eql(admin.passwordSalt);
      });
    });
  });

  it('response JSON should not have password hash', function () {
    _.each([user, admin], function (usr) {
      expect(usr).to.have.property('passwordHash');
      _.each(usr.$toJson(), function (value) {
        expect(value).not.to.eql(user.passwordHash);
        expect(value).not.to.eql(admin.passwordHash);
      });
    });
  });

  it('database should not have password', function () {
    return db('user').then(function (rows) {
      _.each(rows, function (row) {
        _.each(row, function (value) {
          expect(value).not.to.eql(userPassword);
          expect(value).not.to.eql(adminPassword);
        });
      });
    });
  });

  it('should remove user', function () {
    return user.$del().then(function () {
      return db('User').where('id', '=', user.id);
    }).then(function (rows) {
      expect(rows.length).to.eql(0);
      return User.findByUsername(user.username);
    }).then(function (model) {
      expect(model).to.be(undefined);
      return User.findOne().where({id: user.id});
    }).then(function (model) {
      expect(model).to.be(undefined);
    });
  });

  it('should remove admin', function () {
    return admin.$del().then(function () {
      return db('User').where('id', '=', admin.id);
    }).then(function (rows) {
      expect(rows.length).to.eql(0);
      return User.findByUsername(admin.username);
    }).then(function (model) {
      expect(model).to.be(undefined);
      return User.findOne().where({id: user.id});
    }).then(function (model) {
      expect(model).to.be(undefined);
    });
  });

  it('should have role hierarchies', function () {
    expect(admin.$hasRole(User.Role.User)).to.eql(true);
    expect(admin.$hasRole(User.Role.Admin)).to.eql(true);
    expect(user.$hasRole(User.Role.User)).to.eql(true);
    expect(user.$hasRole(User.Role.Admin)).to.eql(false);
  })

});
