"use strict";

var Route = require('./Route');

/**
 * Wrapper for express.Router that makes handlers aware of knex.js transactions and promises.
 *
 * @see Router#get for examples.
 * @constructor
 * @param {express.Router} expressRouter
 * @param {function} [opts.defaultAuthHandler]
 * @param {Number} [opts.unauthenticatedStatusCode]
 * @param {Number} [opts.expressMiddlewareTimeout] Timeout for functions registered using `Route#middleware()`, in milliseconds
 * @param {Number} [opts.authHandlerTimeout] Timeout for functions registered using `Route#auth()`, in milliseconds
 * @param {Number} [opts.handlerTimeout] Timeout for functions registered using `Route#handler()`, in milliseconds
 * @param {Number} [opts.retryCountLimit] Maximum number of retries (after the original handling attempt) when using retryOnError()
 */
function Router(expressRouter, opts) {
  opts = opts || {};

  /**
   * @type {express.Router}
   */
  this.expressRouter = expressRouter;
  /**
   * @type {function(req, transaction):boolean}
   */
  this.defaultAuthHandler = opts.defaultAuthHandler || null;
  /**
   * @type {Number}
   */
  this.unauthenticatedStatusCode = opts.unauthenticatedStatusCode || 401;
  /**
   * @type {Number}
   */
  this.expressMiddlewareTimeout = opts.expressMiddlewareTimeout || null;
  /**
   * @type {Number}
   */
  this.authHandlerTimeout = opts.authHandlerTimeout || null;
  /**
   * @type {Number}
   */
  this.handlerTimeout = opts.handlerTimeout || null;
  /**
   * @type {Number}
   */
  this.retryCountLimit = typeof(opts.retryCountLimit) === 'number' ? opts.retryCountLimit : 5;
}

/**
 * Creates a GET request handler that plays nice with promises and knex.js transactions.
 *
 * This method works like the `get` method of express, but adds the possibility to return
 * a response from the handler either as an object or a promise. No need to explicitly call
 * `res.send`, `res.json` etc.
 *
 * ```js
 * router
 *   .get('/some/path/:id')
 *   .handler(function (req) {
 *     return req.models.SomeSqlModel.findById(req.params.id);
 *   });
 * ```
 *
 * If the request handler takes three parameters it is executed inside a *knex.js*
 * transaction. The transaction object is given to the handler as the third parameter.
 *
 * ```js
 * router
 *   .get('/some/path')
 *   .handler(function (req, res, transaction) {
 *     return req.models.SomeSqlModel
 *       .find()
 *       .transacting(transaction);
 *   });
 * ```
 *
 * The return value doesn't have to be a Promise or Thenable. Anything can be returned
 * from the handler.
 *
 * ```js
 * router
 *   .get('/some/path/:id')
 *   .handler(function (req) {
 *     return {plain: 'old json'};
 *   });
 * ```
 *
 * We can even call the `res.send`, `res.json`, `res.end` etc. methods.
 *
 * ```js
 * router
 *   .get('/some/path/:id')
 *   .handler(function (req, res) {
 *     res.send('just text');
 *   });
 * ```
 *
 * Add `.auth` method call to check if a user is logged in. If no user is logged in a
 * 401 error is sent. This can be overridden with the unauthenticatedStatusCode option.
 *
 * ```js
 * router
 *   .get('/some/path/:id')
 *   .auth()
 *   .handler(function (req) {
 *     // We never get here if no user is logged in.
 *     return {plain: 'old json'};
 *   });
 * ```
 *
 * The `auth` method can also take a function as a parameter. The function should return
 * true/false or a Promise that evaluates to true or false. If false is returned a 403
 * response is sent.
 *
 * ```js
 * router
 *   .get('/some/path/:id')
 *   .auth(function (req, transaction) {
 *     // Authenticate admins. We could also return a promise from here.
 *     return req.user.role === req.models.User.Role.Admin;
 *   })
 *   .handler(function (req, res, transaction) {
 *     // This is only executed if the `auth` function returned true.
 *   });
 * ```
 *
 * The `auth` and `handler` share a request specific context.
 *
 * ```js
 * router
 *   .get('/some/path/:id')
 *   .auth(function (req, transaction) {
 *     this.foo = 'bar';
 *     return true;
 *   })
 *   .handler(function (req, res, transaction) {
 *     console.log(this.foo); // --> 'bar'
 *     return this;
 *   });
 * ```
 *
 * @param {String} path
 * @returns {Route}
 */
Router.prototype.get = function (path) {
  return this._route(path, 'get');
};

/**
 * Creates a PUT request handler that plays nice with promises and knex.js transactions.
 *
 * @see Router#get For detailed documentation on how to use this method.
 * @param {String} path
 * @returns {Route}
 */
Router.prototype.put = function (path) {
  return this._route(path, 'put');
};

/**
 * Creates a PATCH request handler that plays nice with promises and knex.js transactions.
 *
 * @see Router#get For detailed documentation on how to use this method.
 * @param {String} path
 * @returns {Route}
 */
Router.prototype.patch = function (path) {
  return this._route(path, 'patch');
};

/**
 * Creates a POST request handler that plays nice with promises and knex.js transactions.
 *
 * @see Router#get For detailed documentation on how to use this method.
 * @param {String} path
 * @returns {Route}
 */
Router.prototype.post = function (path) {
  return this._route(path, 'post');
};

/**
 * Creates a DELETE request handler that plays nice with promises and knex.js transactions.
 *
 * @see Router#get For detailed documentation on how to use this method.
 * @param {String} path
 * @returns {Route}
 */
Router.prototype.delete = function (path) {
  return this._route(path, 'delete');
};

/**
 * @private
 */
Router.prototype._route = function (path, method) {
  return new Route({
    path: path,
    method: method,
    expressRouter: this.expressRouter,
    defaultAuthHandler: this.defaultAuthHandler,
    unauthenticatedStatusCode: this.unauthenticatedStatusCode,
    expressMiddlewareTimeout: this.expressMiddlewareTimeout,
    authHandlerTimeout: this.authHandlerTimeout,
    handlerTimeout: this.handlerTimeout,
    retryCountLimit: this.retryCountLimit
  });
};

module.exports = Router;
